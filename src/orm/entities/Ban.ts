import { Column, Entity, PrimaryColumn } from 'typeorm'
import { Snowflake } from '../../discordApiTypes'

@Entity('Ban')
export class Ban {
	@PrimaryColumn({ type: 'bigint', nullable: false })
	public userId: Snowflake
	@Column({ type: 'text', nullable: false })
	public reason: string
}
