import { EmbedBuilder, SlashCommandUserOption } from '@discordjs/builders'
import assert from 'assert/strict'
import { client } from '../../bot'
import { CmdContext } from '../../context'
import { ApplicationCommandOptionType, MessageFlags, Snowflake } from '../../discordApiTypes'
import { fromRankedPlayers, Player } from '../../orm/entities/Player'
import { getDisplayName } from '../../utils'
import { Command, CommandType } from '../command'

const command: Command = {
	name: 'stats',
	type: CommandType.GAME,
	usableIfBanned: true,
	requireExternalEmojis: false,

	buildOptions(builder, t) {
		builder.addUserOption(
			new SlashCommandUserOption()
				.setName('user')
				.setDescription(t('cmd.stats.user_description'))
				.setRequired(false)
		)
	},

	async exec(ctx: CmdContext): Promise<void> {
		const userOpt = ctx.interaction.data.options?.find(o => o.name === 'user')
		const userId = userOpt !== undefined && userOpt.type === ApplicationCommandOptionType.User
			? userOpt.value
			: ctx.userId

		const self = userId === ctx.userId

		const player = await ctx.tx.findOne(Player, userId)

		if (!self && player?.anonymous) {
			await client.interactionReply(ctx, {
				content: ctx.p.t('cmd.stats.anonymous'),
				flags: MessageFlags.Ephemeral,
			})
			return
		}

		const { rank } = await ctx.tx.createQueryBuilder()
			.select('rank')
			.from(qb => fromRankedPlayers(qb)
					.select('id')
					.addSelect('rank() over (ORDER BY elo DESC)', 'rank')
				, 'T')
			.where('id = :userId', { userId })
			.getRawOne<{ id: Snowflake, rank: string }>() || {}

		if (!rank) {
			await client.interactionReply(ctx, {
				content: ctx.p.t(`cmd.stats.not_ranked.${self ? 'you' : 'he'}`),
				flags: MessageFlags.Ephemeral,
			})
			return
		}

		assert(player !== undefined)

		const displayName = await getDisplayName(userId, ctx)
		const winRate = Math.round(100 * player.winRate) + '%'

		await client.interactionReply(ctx, {
			embeds: [
				new EmbedBuilder()
					.setTitle(ctx.p.t('cmd.stats.embed.title', { displayName }))
					.setFields([
						{ name: ctx.p.t('cmd.stats.embed.rank')    , value: rank                             , inline: true },
					    { name: ctx.p.t('cmd.stats.embed.elo')     , value: player.elo.toString()            , inline: true },
					    { name: ctx.p.t('cmd.stats.embed.win_rate'), value: winRate                          , inline: true },
					    { name: ctx.p.t('cmd.stats.embed.wins')    , value: player.wins.toString()           , inline: true },
					    { name: ctx.p.t('cmd.stats.embed.loses')   , value: player.loses.toString()          , inline: true },
					    { name: ctx.p.t('cmd.stats.embed.ties')    , value: player.ties.toString()           , inline: true },
					    { name: ctx.p.t('cmd.stats.embed.games')   , value: player.nbOfPlayedGames.toString(), inline: true },
					])
					.toJSON()
			],
			flags: MessageFlags.Ephemeral,
		})
	},
}

export default command
