import { client } from '../../bot'
import { CmdContext } from '../../context'
import { MessageFlags } from '../../discordApiTypes'
import { Player } from '../../orm/entities/Player'
import { Command, CommandType } from '../command'

const command: Command = {
	name: 'anon',
	type: CommandType.UTILITY,
	usableIfBanned: true,
	requireExternalEmojis: false,

	async exec(ctx: CmdContext): Promise<void> {
		const player = await Player.createIfDoesNotExist(ctx.userId, ctx.tx)
		player.anonymous = !player.anonymous
		await ctx.tx.save(player)

		await client.interactionReply(ctx, {
			content: ctx.p.t(`cmd.anon.${player.anonymous ? 'on' : 'off'}`),
			flags: MessageFlags.Ephemeral,
		})
	},
}

export default command
