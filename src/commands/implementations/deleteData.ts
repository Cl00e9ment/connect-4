import { ButtonBuilder } from '@discordjs/builders'
import { client } from '../../bot'
import { BtnContext, CmdContext } from '../../context'
import { ButtonStyle, MessageFlags } from '../../discordApiTypes'
import { Game } from '../../orm/entities/Game'
import { Player } from '../../orm/entities/Player'
import { Command, CommandType, organiseButtons } from '../command'

const command: Command = {
	name: 'delete-data',
	type: CommandType.UTILITY,
	usableIfBanned: true,
	requireExternalEmojis: false,

	async exec(ctx: CmdContext): Promise<void> {
		const player = await ctx.tx.findOne(Player, ctx.userId)

		if (!player) {
			await client.interactionReply(ctx, {
				content: ctx.p.t('cmd.delete-data.not_registered'),
				flags: MessageFlags.Ephemeral,
			})
			return
		}

		await client.interactionReply(ctx, {
			content: ctx.p.t('cmd.delete-data.confirm.msg'),
			components: organiseButtons([
				new ButtonBuilder()
					.setCustomId('confirm')
					.setStyle(ButtonStyle.Danger)
					.setLabel(ctx.p.t('cmd.delete-data.confirm.btn'))
			]),
			flags: MessageFlags.Ephemeral,
		})
	},

	async onBtnClicked(ctx: BtnContext): Promise<void> {
		const count = await ctx.tx.count(Player, {
			where: {
				id: ctx.userId,
			},
		})

		if (count === 0) {
			await client.componentInteractionUpdateMsg(ctx.interaction, {
				content: ctx.p.t('cmd.delete-data.not_registered'),
				components: [],
				flags: MessageFlags.Ephemeral,
			})
			return
		}

		await ctx.tx.createQueryBuilder()
			.update(Game)
			.set({ player0: null })
			.where('"player0Id" = :playerId', { playerId: ctx.userId })
			.execute()

		await ctx.tx.createQueryBuilder()
			.update(Game)
			.set({ player1: null })
			.where('"player1Id" = :playerId', { playerId: ctx.userId })
			.execute()

		await ctx.tx.delete(Player, ctx.userId)

		await client.componentInteractionUpdateMsg(ctx.interaction, {
			content: ctx.p.t('cmd.delete-data.validation'),
			components: [],
			flags: MessageFlags.Ephemeral,
		})
	},
}

export default command
