import { Column, Entity, EntityManager, PrimaryColumn } from 'typeorm'
import { config } from '../../config'
import { fallbackLocale, translate } from '../../intl'
import { PrimitiveType } from '../../utils'
import { Snowflake } from '../../discordApiTypes'

export const maxUrlLength = 2000

export async function getPreferences(guildId: Snowflake | null, transaction: EntityManager) {
	if (guildId === null) {
		return new Preferences();
	}
	let p = await transaction.findOne(Preferences, guildId)
	if (p == null) {
		p = new Preferences()
		p.guildId = guildId;
	}
	return p
}

@Entity('Preferences')
export class Preferences {
	@PrimaryColumn({ type: 'bigint', nullable: false })
	public guildId: Snowflake
	@Column({ type: 'varchar', length: 8, nullable: true })
	public locale: string | null = null
	@Column({ type: 'varchar', length: maxUrlLength, nullable: true })
	public winImgUrl: string | null = null
	@Column({ type: 'varchar', length: maxUrlLength, nullable: true })
	public tieImgUrl: string | null = null

	public get effectiveLocale(): string {
		return this.locale ?? fallbackLocale
	}

	public get effectiveWinImgUrl(): string {
		return this.winImgUrl ?? config.default_game_over_img.win
	}

	public get effectiveTieImgUrl(): string {
		return this.tieImgUrl ?? config.default_game_over_img.tie
	}

	public t(key: string, options?: Record<string, PrimitiveType> | number): string {
		return translate(this.effectiveLocale, key, options)
	}
}
