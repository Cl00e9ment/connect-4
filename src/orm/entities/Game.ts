import { bold, ButtonBuilder, EmbedBuilder, italic, messageLink, RGBTuple, userMention } from '@discordjs/builders'
import assert from 'assert/strict'
import { bufferPadStart } from 'pad-buffer'
import {
	AfterLoad,
	BeforeInsert,
	BeforeUpdate,
	Column,
	Entity,
	JoinColumn,
	ManyToOne,
	PrimaryGeneratedColumn
} from 'typeorm'
import { organiseButtons } from '../../commands/command'
import { BtnContext, Context } from '../../context'
import { discs } from '../../disc'
import {
	APIInteractionResponseCallbackData,
	ButtonStyle,
	RESTPostAPIChannelMessageJSONBody,
	Snowflake
} from '../../discordApiTypes'
import { Cell, Outcome } from '../../gameUtils'
import { base3, formatNumber, getDisplayName, trySendDM } from '../../utils'
import { Player, StatsUpdate } from './Player'
import { Preferences } from './Preferences'

export enum Status {
	PLAYER_0_INITIALIZED,
	BOTH_PLAYERS_INITIALIZED,
	BOTH_PLAYERS_PLAYED,
	TIE,
	PLAYER_0_WIN,
	PLAYER_0_WIN_FORFEIT,
	PLAYER_1_WIN,
	PLAYER_1_WIN_FORFEIT,
}

@Entity('Game')
export class Game {
	@PrimaryGeneratedColumn({ type: 'int' })
	public id: number

	@Column({ type: 'bytea', nullable: false })
	public board: Buffer

	/**
	 * Null iff account was deleted.
	 */
	@ManyToOne(() => Player, { eager: true })
	@JoinColumn({ name: 'player0Id' })
	public player0: Player | null

	/**
	 * Null iff any of:
	 * - player1 isn't yet initialized (status = 0)
	 * - account was deleted
	 */
	@ManyToOne(() => Player, { eager: true })
	@JoinColumn({ name: 'player1Id' })
	public player1: Player | null

	@Column({ type: 'smallint', nullable: false, default: 0 })
	public turn: number

	@Column({ type: 'smallint', nullable: false })
	public status: Status

	@Column({ type: 'timestamp with time zone', nullable: false })
	public startDatetime = new Date()

	/**
	 * Null iff no move was made (turn = 0).
	 */
	@Column({ type: 'timestamp with time zone', nullable: true })
	public lastMoveDatetime: Date | null

	/**
	 * Null iff any of:
	 * - no move was made (turn = 0)
	 * - last move is not known (last move was canceled or was made before the implementation of this feature)
	 */
	@Column({ type: 'smallint', nullable: true })
	public lastMove: number | null

	@Column({ type: 'smallint', nullable: false })
	public player0Disc: number

	@Column({ type: 'smallint', nullable: true })
	public player1Disc: number | null

	/**
	 * Explanation of the value:
	 * - null: no players voted to cancel the current turn
	 * - 0: player 0 voted to cancel the current turn
	 * - 1: player 1 voted to cancel the current turn
	 */
	@Column({ type: 'smallint', nullable: true })
	public cancelVote: number | null

	@Column({ type: 'bigint', nullable: false, array: true, default: [] })
	public subscribers: Snowflake[]

	public static readonly WIDTH = 7
	public static readonly HEIGHT = 6

	private readonly cells: Cell[][]

	public static isValidId(id: number) {
		return id >= 1 && id <= 2**31 - 1;
	}

	public static async createFromPlayerIds(player0Id: Snowflake, player1Id: Snowflake | null, ctx: Context): Promise<Game> {
		const player0 = await Player.createIfDoesNotExist(player0Id, ctx.tx)
		const player1 = await (async () => {
			if (!player1Id) {
				return null
			}
			if (player1Id === player0Id) {
				return player0
			}
			return await Player.createIfDoesNotExist(player1Id, ctx.tx)
		})()

		const game = new Game()
		game.player0     = player0
		game.player1     = player1
		game.player0Disc = player0.mainDisc

		if (player1) {
			game.player1Disc = player1.chooseDisc(game.player0Disc)
			game.status      = Status.BOTH_PLAYERS_INITIALIZED
		} else {
			game.player1Disc = null
			game.status      = Status.PLAYER_0_INITIALIZED
		}

		await ctx.tx.save(game)
		return game
	}

	private static getOutcomeText(playerId: Snowflake, statsUpdate: StatsUpdate, p: Preferences): string {
		const deltaElo = formatNumber(statsUpdate.deltaElo, true)
		const elo = formatNumber(statsUpdate.newElo)

		const deltaCoins = formatNumber(statsUpdate.deltaCoins, true)
		const coins = formatNumber(statsUpdate.newCoins)

		return `${bold(userMention(playerId))} ${deltaElo} ${p.t('game.elo')} (${elo}) | ${deltaCoins} ${p.t('game.coins', Math.abs(statsUpdate.deltaCoins))} (${coins})`
	}

	public constructor() {
		this.cells = new Array(Game.WIDTH)
			.fill(null).map(() =>
				new Array(Game.HEIGHT).fill(Cell.EMPTY)
			)
	}

	@AfterLoad()
	private normalizePlayers() {
		if (this.player0 && this.player1 && this.player0.id === this.player1.id) {
			this.player1 = this.player0
		}
	}

	@AfterLoad()
	private decodeBoard() {
		let boardStr = base3.encode(this.board)
		boardStr = boardStr.padStart(Game.WIDTH * Game.HEIGHT, '0')
		let i = 0
		for (let x = 0; x < Game.WIDTH; x++) {
			for (let y = 0; y < Game.HEIGHT; y++) {
				this.cells[x][y] = parseInt(boardStr.charAt(i++), 3)
			}
		}
	}

	@BeforeInsert()
	@BeforeUpdate()
	private encodeBoard() {
		let boardStr = ''
		for (let x = 0; x < Game.WIDTH; x++) {
			for (let y = 0; y < Game.HEIGHT; y++) {
				// don't write leading zeros
				if (boardStr.length > 0 || this.cells[x][y] !== Cell.EMPTY) {
					boardStr += this.cells[x][y]
				}
			}
		}
		this.board = bufferPadStart(base3.decode(boardStr), 9)
	}

	public async asInteractionResponse(ctx: Context, statsUpdates: StatsUpdate[] | null = null): Promise<APIInteractionResponseCallbackData> {
		return {
			embeds: [(await this.getEmbed(statsUpdates, ctx)).toJSON()],
			components: organiseButtons(this.getButtons(ctx.p)),
		}
	}

	public isPlayer(userId: Snowflake): boolean {
		return this.player0?.id === userId || this.player1?.id === userId
	}

	public isSubscribed(userId: Snowflake): boolean {
		return this.subscribers.includes(userId)
	}

	public async subscribe(ctx: Context) {
		this.subscribers.push(ctx.userId)
		await ctx.tx.save(this)
	}

	public async unsubscribe(ctx: Context) {
		if (this.unsubscribeNoSave(ctx.userId)) {
			await ctx.tx.save(this)
		}
	}

	private unsubscribeNoSave(userId: Snowflake): boolean {
		const index = this.subscribers.indexOf(userId)
		if (index === -1) {
			return false
		}
		this.subscribers.splice(index, 1)
		return true
	}

	private async broadcast(playerMsgKey: string | null, spectatorMsgKey: string | null, ctx: BtnContext) {
		if (ctx.guild === null || this.subscribers.length === 0) {
			return
		}

		assert(this.player0 !== null)

		const currentPlayerId = ctx.userId
		const opponentId = (() => {
			if (this.status < Status.BOTH_PLAYERS_INITIALIZED) {
				return null
			} else {
				assert(this.player1 !== null)
				return currentPlayerId === this.player0.id ? this.player1.id : this.player0.id
			}
		})()

		const [player0DisplayName, player1DisplayName] = await Promise.all([
			this.getPlayer0DisplayName(ctx),
			this.getPlayer1DisplayName(ctx),
		])
		const currentPlayerDisplayName = currentPlayerId === this.player0.id ? player0DisplayName : player1DisplayName

		const boardUrl = messageLink(ctx.interaction.channel_id, ctx.interaction.message.id, ctx.guild.id)

		const getMsgPayload: (msgKey: string) => RESTPostAPIChannelMessageJSONBody = (msgKey) => ({
			content: ctx.p.t(`subscription.${msgKey}`, {
				player0: player0DisplayName,
				player1: player1DisplayName,
				player: currentPlayerDisplayName,
			}),
			components: organiseButtons([
				new ButtonBuilder()
					.setStyle(ButtonStyle.Link)
					.setLabel('Go')
					.setURL(boardUrl),
			])
		})

		if (playerMsgKey !== null && opponentId !== null && this.subscribers.includes(opponentId)) {
			trySendDM(opponentId, getMsgPayload(playerMsgKey)).catch(e => ctx.logger.error(e))
		}

		if (spectatorMsgKey !== null) {
			const msgPayload = getMsgPayload(spectatorMsgKey)

			for (const subscriber of this.subscribers) {
				if (subscriber === currentPlayerId || subscriber === opponentId) {
					continue
				}
				trySendDM(subscriber, msgPayload).catch(e => ctx.logger.error(e))
			}
		}
	}

	public async play(column: number, ctx: BtnContext): Promise<APIInteractionResponseCallbackData | null> {
		if (!this.canUserPlay(ctx.userId, column)) {
			return null
		}

		assert(this.player0 !== null)

		let dmSent = false

		// init player1 if null
		if (this.turn === 1 && !this.player1) {
			if (this.player0.id === ctx.userId) {
				this.player1 = this.player0
				// unsubscribe player if he's playing against himself
				this.unsubscribeNoSave(ctx.userId)
			} else {
				this.player1 = await Player.createIfDoesNotExist(ctx.userId, ctx.tx)
			}
			this.player1Disc = this.player1.chooseDisc(this.player0Disc)
			this.status = Status.BOTH_PLAYERS_INITIALIZED

			await this.broadcast('common.join', 'common.join', ctx)
			dmSent = true
		}

		// update board
		let row = Game.HEIGHT - 1
		while (this.cells[column][row] !== Cell.EMPTY) --row
		this.cells[column][row] = this.turn % 2 + 1
		this.lastMove = column

		// update date
		this.lastMoveDatetime = new Date()

		// reset the cancel vote
		this.cancelVote = null

		// update turn
		++this.turn

		// If turn 0 and 1 are completed, both players have played.
		if (this.turn === 2) {
			this.status = Status.BOTH_PLAYERS_PLAYED
		}

		if (this.isMoveVictorious(column, row)) {
			// set winner to current player (0 or 1)
			this.status = (this.turn - 1) % 2 === 0 ? Status.PLAYER_0_WIN : Status.PLAYER_1_WIN

			await this.broadcast('player.msg_win', 'spectator.msg_win', ctx)
			dmSent = true;

		} else if (this.boardFilled) {
			// set winner to no one (tie)
			this.status = Status.TIE

			await this.broadcast('player.msg_tie', 'spectator.msg_tie', ctx)
			dmSent = true;
		}

		let statsUpdate = null
		if (this.gameOver) {
			// if the game ended, update elo, update loses/ties/wins counts, and save players
			statsUpdate = await this.processGameOutcome(ctx)
		}

		// save state
		await ctx.tx.save(this)

		if (!dmSent) {
			await this.broadcast('common.msg_move', 'common.msg_move', ctx)
		}

		// update response
		return await this.asInteractionResponse(ctx, statsUpdate)
	}

	public async forfeit(ctx: BtnContext): Promise<APIInteractionResponseCallbackData | null> {
		if (!this.canUserForfeit(ctx.userId)) {
			return null
		}

		const res = await this.silentForfeit(ctx.userId, ctx)
		await this.broadcast('player.msg_forfeit', 'spectator.msg_win', ctx)

		return res
	}

	public async silentForfeit(userId: Snowflake, ctx: Context): Promise<APIInteractionResponseCallbackData> {
		assert(this.canUserForfeit(userId))
		assert(this.player0 !== null)

		this.lastMove = null
		this.status = userId === this.player0.id ? Status.PLAYER_1_WIN_FORFEIT : Status.PLAYER_0_WIN_FORFEIT

		const statsUpdates = await this.processGameOutcome(ctx)
		await ctx.tx.save(this)
		return await this.asInteractionResponse(ctx, statsUpdates)
	}

	public async voteCancelTurn(ctx: BtnContext): Promise<APIInteractionResponseCallbackData | null> {
		if (!this.canUserVoteCancelTurn(ctx.userId)) {
			return null
		}

		assert(this.player0 !== null)

		if (this.player1 === null || this.player0 === this.player1) {
			await this.cancelTurn()

		} else {
			const playerIndex = ctx.userId === this.player0.id ? 0 : 1

			// player has already voted
			if (this.cancelVote === playerIndex) {
				return null
			}

			if (this.cancelVote === null) {
				// player is the first to vote
				this.cancelVote = playerIndex
				await this.broadcast('player.msg_cancel', null, ctx)
			} else {
				// both players have voted to cancel, cancel
				await this.cancelTurn()
				this.cancelVote = null
			}
		}

		// save state
		await ctx.tx.save(this)

		// send next message
		return await this.asInteractionResponse( ctx)
	}

	private async cancelTurn(): Promise<void> {
		assert(this.lastMove !== null)

		// update board
		const column = this.cells[this.lastMove]
		column[column.lastIndexOf(0) + 1] = 0

		// update lastMove
		this.lastMove = null

		// update date
		this.lastMoveDatetime = new Date()

		// update turn
		--this.turn

		// update status
		if (this.turn < 2 && this.status === Status.BOTH_PLAYERS_PLAYED) {
			this.status = Status.BOTH_PLAYERS_INITIALIZED
		}
	}

	private canUserPlay(userId: Snowflake, column: number): boolean {
		// check if game isn't already terminated
		// check if game isn't archived
		// check if it's the turn of the user
		// check column index
		// check if column still have empty cells
		return !this.gameOver
			&& !this.archived
			&& (this.status === Status.PLAYER_0_INITIALIZED && this.turn === 1 || this.currentPlayer?.id === userId)
			&& column >= 0 && column < Game.WIDTH
			&& this.cells[column][0] === Cell.EMPTY
	}

	private canUserForfeit(userId: Snowflake): boolean {
		// check if game isn't already terminated
		// check if game isn't archived
		// check that the player is in the game
		// check if both players played at least one turn
		return !this.gameOver
			&& !this.archived
			&& (this.player0?.id === userId || this.player1?.id === userId)
			&& this.status === Status.BOTH_PLAYERS_PLAYED
	}

	private canUserVoteCancelTurn(userId: Snowflake): boolean {
		// check that the game isn't already terminated
		// check if game isn't archived
		// check that move can be canceled
		// check that the player is in the game
		return !this.gameOver
			&& !this.archived
			&& this.lastMove !== null
			&& (this.player0?.id === userId || this.player1?.id === userId)
	}

	private getButtons(p: Preferences): ButtonBuilder[] {
		if (this.gameOver || this.archived) {
			return []
		}

		const btnIdPrefix = `game${this.id}_turn${this.turn}_`
		const buttons: ButtonBuilder[] = []

		for (let i = 0; i < Game.WIDTH; i++) {
			buttons.push(new ButtonBuilder()
				.setCustomId(btnIdPrefix + 'play' + i)
				.setStyle(ButtonStyle.Primary)
				.setLabel((i + 1).toString())
				.setDisabled(this.cells[i][0] !== Cell.EMPTY)
			)
		}

		buttons.push(new ButtonBuilder()
			.setCustomId(btnIdPrefix + 'bell')
			.setStyle(ButtonStyle.Secondary)
			.setEmoji({ name: '🔔' })
		)

		buttons.push(new ButtonBuilder()
			.setCustomId(btnIdPrefix + 'cancel')
			.setStyle(ButtonStyle.Danger)
			.setLabel(p.t('game.' + (this.cancelVote === null ? 'cancel' : 'cancel_progress')))
			.setDisabled(this.lastMove === null)
		)

		buttons.push(new ButtonBuilder()
			.setCustomId(btnIdPrefix + 'forfeit')
			.setStyle(ButtonStyle.Danger)
			.setLabel(p.t('game.forfeit'))
			.setDisabled(this.status !== Status.BOTH_PLAYERS_PLAYED)
		)

		return buttons
	}

	private async processGameOutcome(ctx: Context): Promise<StatsUpdate[] | null> {
		assert(this.player0 !== null)
		assert(this.player1 !== null)

		if (this.player0.id === this.player1.id) return null

		let outcome: Outcome[]

		switch (this.status) {
			case Status.PLAYER_0_WIN:
			case Status.PLAYER_0_WIN_FORFEIT:
				outcome = [Outcome.WIN, Outcome.LOOSE]
				break

			case Status.PLAYER_1_WIN:
			case Status.PLAYER_1_WIN_FORFEIT:
				outcome = [Outcome.LOOSE, Outcome.WIN]
				break

			case Status.TIE:
				outcome = [Outcome.TIE, Outcome.TIE]
				break

			default:
				assert(false)
		}

		// these values must be computed before updating elo
		const expectedScores = [
			this.player0.getExpectedScore(this.player1),
			this.player1.getExpectedScore(this.player0),
		]

		const statsUpdates = [
			this.player0.processGameOutcome(expectedScores[0], outcome[0]),
			this.player1.processGameOutcome(expectedScores[1], outcome[1]),
		]

		await ctx.tx.save([this.player0, this.player1])

		return statsUpdates
	}

	private async getEmbed(statsUpdates: StatsUpdate[] | null = null, ctx: Context): Promise<EmbedBuilder> {
		const embed = new EmbedBuilder()
			.setTitle(await this.getEmbedTitle(ctx))
			.setColor(this.getEmbedColor())

		if (this.gameOver) {
			let text = bold(ctx.p.t('game.game_id') + ctx.p.t('utils.colon')) + ' ' + this.id.toString()

			if (statsUpdates !== null) {
				assert(this.player0 !== null)
				assert(this.player1 !== null)

				const outcomeTexts = await Promise.all([
					Game.getOutcomeText(this.player0.id, statsUpdates[0], ctx.p),
					Game.getOutcomeText(this.player1.id, statsUpdates[1], ctx.p),
				])
				text += '\n' + outcomeTexts[0]
				text += '\n' + outcomeTexts[1]
			}

			text += '\n\n' + this.buildTextBoard()

			embed
				.setDescription(text)
				.setImage(this.getEmbedImage(ctx.p))

		} else {
			let text =
				  bold(ctx.p.t('game.game_id') + ctx.p.t('utils.colon')) + ' ' + this.id.toString() + '\n'
				+ bold(ctx.p.t('game.turn') + ctx.p.t('utils.colon')) + ' ' + await this.getCurrentPlayerDisplayName(ctx, true) + '\n'
				+ '\n'
				+ this.buildTextBoard()

			if (this.archived) {
				text += '\n' + ctx.p.t('game.archived')
			}

			embed.setDescription(text)
		}

		return embed
	}

	private async getEmbedTitle(ctx: Context): Promise<string> {
		if (this.gameOver) {
			if (this.status === Status.TIE) {
				return ctx.p.t('game.tie')
			} else {
				const winner = await this.getWinnerDisplayName(ctx)
				return ctx.p.t(this.isForfeit ? 'game.win_forfeit' : 'game.win', { winner })
			}
		} else {
			const playerNames = await Promise.all([
				this.getPlayer0DisplayName(ctx),
				this.getPlayer1DisplayName(ctx),
			])
			return `${playerNames[0]} ⚡ ${playerNames[1]}`
		}
	}

	private getEmbedColor(): number | RGBTuple | null {
		if (this.archived) return null // default color

		switch (this.status) {
			case Status.TIE:
				return null // default color

			case Status.PLAYER_0_WIN:
			case Status.PLAYER_0_WIN_FORFEIT:
				return discs[this.player0Disc].embedColor

			case Status.PLAYER_1_WIN:
			case Status.PLAYER_1_WIN_FORFEIT:
				assert(this.player1Disc !== null)
				return discs[this.player1Disc].embedColor

			default:
				if (this.turn % 2 === 0) {
					return discs[this.player0Disc].embedColor
				} else if (this.status >= Status.BOTH_PLAYERS_INITIALIZED) {
					assert(this.player1Disc !== null)
					return discs[this.player1Disc].embedColor
				} else {
					return null // default color
				}
		}
	}

	private getEmbedImage(p: Preferences): string | null {
		if (this.gameOver) {
			return this.status === Status.TIE ? p.effectiveTieImgUrl : p.effectiveWinImgUrl
		} else {
			return null
		}
	}

	private async getPlayer0DisplayName(ctx: Context, mention = false): Promise<string> {
		if (!this.player0) {
			return italic(ctx.p.t('utils.deleted'))
		}

		if (mention) {
			return userMention(this.player0.id)
		}

		return await getDisplayName(this.player0.id, ctx)
	}

	private async getPlayer1DisplayName(ctx: Context, mention = false): Promise<string> {
		if (!this.player1) {
			if (this.status < Status.BOTH_PLAYERS_INITIALIZED) {
				return ctx.p.t('game.whoever_join')
			} else {
				return italic(ctx.p.t('utils.deleted'))
			}
		}

		if (mention) {
			return userMention(this.player1.id)
		}

		return await getDisplayName(this.player1.id, ctx)
	}

	private async getCurrentPlayerDisplayName(ctx: Context, mention = false): Promise<string> {
		if (this.turn % 2 === 0) {
			return await this.getPlayer0DisplayName(ctx, mention)
		} else {
			return await this.getPlayer1DisplayName(ctx, mention)
		}
	}

	private async getWinnerDisplayName(ctx: Context): Promise<string | null> {
		switch (this.status) {
			case Status.PLAYER_0_WIN:
			case Status.PLAYER_0_WIN_FORFEIT:
				return await this.getPlayer0DisplayName(ctx)

			case Status.PLAYER_1_WIN:
			case Status.PLAYER_1_WIN_FORFEIT:
				return await this.getPlayer1DisplayName(ctx)

			default:
				return null
		}
	}

	private buildTextBoard(): string {
		let board = ''

		if (this.lastMove !== null) {
			for (let x = 0; x < Game.WIDTH; ++x) {
				board += x === this.lastMove
					? '<:4:955050138110427217>' // arrow down
					: '<:4:989608964088029224>' // small black square
			}
			board += '\n'
		}

		for (let y = 0; y < Game.HEIGHT; y++) {
			for (let x = 0; x < Game.WIDTH; x++) {
				switch (this.cells[x][y]) {
					case Cell.EMPTY:
						board += '<:4:989608965895778324>' // black disc
						break
					case Cell.PLAYER_0:
						board += discs[this.player0Disc].emoji
						break
					case Cell.PLAYER_1:
						assert(this.player1Disc !== null)
						board += discs[this.player1Disc].emoji
						break
				}
			}
			board += '\n'
		}

		if (!this.gameOver && !this.archived) {
			// display column numbers
			board += '\n'
				+ '<:4:955050138198487041>'
				+ '<:4:955050138173317120>'
				+ '<:4:955050138429165578>'
				+ '<:4:955050138265583646>'
				+ '<:4:955050138143973376>'
				+ '<:4:955050138534019122>'
				+ '<:4:955050138177527818>'
		}

		return board
	}

	/**
	 * Check if the last play wins the game.
	 * Warning: this function doesn't check all the board.
	 *
	 * @param x X position of the last played cell
	 * @param y Y position of the last played cell
	 * @private
	 */
	private isMoveVictorious(x: number, y: number) {
		const player = this.cells[x][y]

		// check line
		const lineMin = Math.max(0, x - 3)
		const lineMax = Math.min(Game.WIDTH - 1, x + 3)
		let count = 0
		for (let i = lineMin; i <= lineMax; ++i) {
			if (this.cells[i][y] === player) ++count
			else count = 0
			if (count === 4) return true
		}

		// check column
		const columnMin = Math.max(0, y - 3)
		const columnMax = Math.min(Game.HEIGHT - 1, y + 3)
		count = 0
		for (let i = columnMin; i <= columnMax; ++i) {
			if (this.cells[x][i] === player) ++count
			else count = 0
			if (count === 4) return true
		}

		// check 1st diagonal
		let diagonalMin = Math.max(lineMin - x, columnMin - y)
		let diagonalMax = Math.min(lineMax - x, columnMax - y)
		count = 0
		for (let i = diagonalMin; i <= diagonalMax; ++i) {
			if (this.cells[x + i][y + i] === player) ++count
			else count = 0
			if (count === 4) return true
		}

		// check 2nd diagonal
		diagonalMin = Math.max(lineMin - x, y - columnMax)
		diagonalMax = Math.min(lineMax - x, y - columnMin)
		count = 0
		for (let i = diagonalMin; i <= diagonalMax; ++i) {
			if (this.cells[x + i][y - i] === player) ++count
			else count = 0
			if (count === 4) return true
		}
	}

	public get gameOver(): boolean {
		switch (this.status) {
			case Status.TIE:
			case Status.PLAYER_0_WIN:
			case Status.PLAYER_0_WIN_FORFEIT:
			case Status.PLAYER_1_WIN:
			case Status.PLAYER_1_WIN_FORFEIT:
				return true
			default:
				return false
		}
	}

	public get currentPlayer(): Player | null {
		return this.turn % 2 === 0 ? this.player0 : this.player1
	}

	private get isForfeit(): boolean {
		switch (this.status) {
			case Status.PLAYER_0_WIN_FORFEIT:
			case Status.PLAYER_1_WIN_FORFEIT:
				return true
			default:
				return false
		}
	}

	private get boardFilled(): boolean {
		return this.turn === Game.WIDTH * Game.HEIGHT
	}

	public get archived(): boolean {
		// one or more player is missing -> the game can't continue and is in an archived state
		return !this.player0 || (!this.player1 && this.status >= Status.BOTH_PLAYERS_INITIALIZED)
	}
}
