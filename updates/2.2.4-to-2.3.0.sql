alter table "Preferences"
    rename column lang to locale;

alter table "Preferences"
    alter column locale type varchar(8) using locale::varchar(8);

update "Preferences" set locale = 'en-US' where locale = 'en';
update "Preferences" set locale = 'fr-FR' where locale = 'fr';

-- normalize time zones (these commands should be adapted according to non standard time zones present in DB)
update "Preferences" set tz = 'UTC'                 where tz in ('GMT', 'Zulu', 'Universal');
update "Preferences" set tz = 'America/Chicago'     where tz in ('US/Central', 'CST6CDT');
update "Preferences" set tz = 'America/New_York'    where tz in ('US/Eastern', 'Etc/GMT-5', 'EST');
update "Preferences" set tz = 'America/Denver'      where tz = 'US/Mountain';
update "Preferences" set tz = 'America/Detroit'     where tz = 'US/Michigan';
update "Preferences" set tz = 'America/Los_Angeles' where tz = 'US/Pacific';
update "Preferences" set tz = 'America/Phoenix'     where tz = 'US/Arizona';
update "Preferences" set tz = 'America/Toronto'     where tz = 'Canada/Eastern';
update "Preferences" set tz = 'Asia/Bangkok'        where tz = 'Etc/GMT+7';
update "Preferences" set tz = 'Asia/Singapore'      where tz = 'Singapore';
update "Preferences" set tz = 'Australia/Sydney'    where tz = 'Australia/NSW';
