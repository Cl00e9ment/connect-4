import { translatorDiscIndex } from '../disc'
import { Snowflake } from '../discordApiTypes'

export const adminToolsSocketPath = '/tmp/admin-tools-ipc'

export class Backend {
	public async doesUserExist(userId: Snowflake): Promise<boolean> {
		const { client } = await import('../bot')

		try {
			await client.fetchUser(userId)
			return true
		} catch (e) {
			if (e.httpStatus !== 404) {
				throw e
			}
			return false
		}
	}

	public async isUserBanned(userId: Snowflake): Promise<false | string> {
		const { getManager } = await import('typeorm')
		const { Ban } = await import('../orm/entities/Ban')

		const ban = await getManager().findOne(Ban, userId);
		return ban?.reason ?? false
	}

	public async banUser(userId: Snowflake, reason: string): Promise<void> {
		const { getManager } = await import('typeorm')
		const { Ban } = await import('../orm/entities/Ban')

		const ban = new Ban()
		ban.userId = userId
		ban.reason = reason
		await getManager().save(ban)
	}

	public async unbanUser(userId: Snowflake): Promise<void> {
		const { getManager } = await import('typeorm')
		const { Ban } = await import('../orm/entities/Ban')

		await getManager().delete(Ban, userId)
	}

	public async toggleMaintenance(): Promise<boolean> {
		const { isMaintenanceEnabled, toggleMaintenance } = await import('../bot')

		toggleMaintenance()
		return isMaintenanceEnabled()
	}

	public async getCommandsName(): Promise<string[]> {
		const { getCommandsName } = await import('../commands/manager')
		return getCommandsName()
	}

	public async reloadAllCommands(): Promise<void> {
		const { updateSlashCommands } = await import('../commands/manager')
		await updateSlashCommands()
	}

	public async reloadACommand(commandName: string): Promise<void> {
		const { updateSlashCommand } = await import('../commands/manager')
		await updateSlashCommand(commandName)
	}

	public async pullTranslation(): Promise<void> {
		const { initIntl } = await import('../intl')
		await initIntl()
	}

	public async giveTranslatorDisc(userId: Snowflake): Promise<void> {
		const { getConnection } = await import ('typeorm')
		const { Player } = await import ('../orm/entities/Player')

		await getConnection().transaction(async tx => {
			const player = await Player.createIfDoesNotExist(userId, tx);
			player.giveDisc(translatorDiscIndex)
			await tx.save(player)
		})
	}

	public async giveGems(userId: Snowflake, quantity: number): Promise<void> {
		const { getConnection } = await import ('typeorm')
		const { Player } = await import ('../orm/entities/Player')

		await getConnection().transaction(async tx => {
			const player = await Player.createIfDoesNotExist(userId, tx);
			player.gems += quantity
			await tx.save(player)
		})
	}
}
