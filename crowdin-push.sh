#!/bin/bash

#######################################################################
# How to execute this script?                                         #
# In DEVELOPMENT env, set the CROWDIN_API_TOKEN env var and then run: #
# pnpm run crowdin-push                                               #
#######################################################################

TMP_DIR='/tmp/intl'
CROWDIN_PROJECT_ID='497159'

set -e

if [ -z "$CROWDIN_API_TOKEN" ]; then
	echo "CROWDIN_API_TOKEN env var isn't set" >&2
	exit 1
fi

# reset tmp dir
rm -rf "$TMP_DIR"
mkdir -p "$TMP_DIR"

# create strings.json
STRINGS_PATH="${TMP_DIR}/strings.json"
node -e "
const { readFileSync, writeFileSync } = require('fs')
const { parse } = require('yaml')
writeFileSync('${STRINGS_PATH}', JSON.stringify(parse(readFileSync('intl/source.yml', 'utf8')), null, '\t') + '\n')
"

# create Crowdin config
# to not un-approve translations, use update_option: update_without_changes
CROWDIN_CONFIG_PATH="${TMP_DIR}/crowdin.yml"
echo "
project_id: '${CROWDIN_PROJECT_ID}'
api_token_env: 'CROWDIN_API_TOKEN'
base_path: '${TMP_DIR}'
base_url: 'https://api.crowdin.com'
files:
- source: '/strings.json'
  translation: '/%locale%.json'
  update_option: 'update_as_unapproved'
" > "$CROWDIN_CONFIG_PATH"

# upload strings to Crowdin
crowdin push sources --config "$CROWDIN_CONFIG_PATH"

# cleanup
rm -rf "$TMP_DIR"
