Privacy policy of the Connect 4 Discord bot
-------------------------------------------------------------------------------
publication date: 2022-10-22
last modification date: 2022-12-11


1. Stored data

  1.1 Stored servers data

If a server manager modifies the bot preferences using the web dashboard. The
modified settings will be stored, such as:

- the language
- the win image URL
- the tie image URL

  1.2 Stored users data

When a user interacts with the bot, the following data may be stored:

- the number of elo points
- the number of loses, ties, and wins
- whether the user want to be anonymous
- the main disc and secondary disc used, as well as all purchased discs
- the number of coins and gems
- the last time the user voted for the bot on tog.gg, and the number of
  consecutive votes
- the games played

2. Data deletion

  2.1 Servers data deletion

Kicking the bot from a server will immediately delete all data related to this
server.

  2.2 Users data deletion

Using the /delete-data command will delete all data related to your account.
This includes your elo points, as well as your placement on the leaderboard.
