import { client } from '../../bot'
import { config } from '../../config'
import { CmdContext } from '../../context'
import { MessageFlags } from '../../discordApiTypes'
import { Command, CommandType } from '../command'

const command: Command = {
	name: 'support',
	type: CommandType.UTILITY,
	usableIfBanned: true,
	requireExternalEmojis: false,

	async exec(ctx: CmdContext): Promise<void> {
		await client.interactionReply(ctx, {
			content: config.support_server ? `https://discord.gg/${config.support_server.invite_code}` : ctx.p.t('cmd.support.no_server'),
			flags: MessageFlags.Ephemeral,
		})
	},
}

export default command
