export enum Cell {
	EMPTY,
	PLAYER_0,
	PLAYER_1,
}

export enum Outcome {
	LOOSE,
	TIE,
	WIN,
}

export function outcomeToScore(outcome: Outcome): number {
	return outcome / 2
}
