import { DiscordSnowflake } from '@sapphire/snowflake'
import { EntityManager, getConnection, getManager } from 'typeorm'
import { Client } from './client'
import { processCommandInteraction, processComponentInteraction, processModalInteraction } from './commands/manager'
import { config } from './config'
import { CmdContext, ComponentContext, execInContext, ModalContext } from './context'
import {
	ActivityType,
	ApplicationCommandType,
	GatewayDispatchEvents,
	GatewayIntentBits,
	GatewayInteractionCreateDispatchData,
	InteractionType,
	Snowflake
} from './discordApiTypes'
import { getLogger, mainLogger } from './logger'
import { Preferences } from './orm/entities/Preferences'
import { startTopggIntegration } from './topgg'
import { shallowDump } from './utils'

const guildDeleteLogger = getLogger('guildDelete hook')
let maintenance = false

export const client: Client = new Client({
	botToken: config.bot_token,
	intents: GatewayIntentBits.Guilds | GatewayIntentBits.GuildMembers,
	activity: {
		type: ActivityType.Playing,
		name: '/help',
	},
})

export async function startBot(): Promise<void> {
	await client.connect()

	client.on(GatewayDispatchEvents.InteractionCreate, onInteraction)
	client.on(GatewayDispatchEvents.GuildDelete, g => onGuildDelete(g.id, getManager()))

	mainLogger.info('Bot is ready!')

	await guildCleanup()

	// being extra-precautious because hard to test in development environment
	try {
		await startTopggIntegration()
	} catch (err) {
		// not a big deal if top.gg integration doesn't work, just log the issue
		mainLogger.error(err)
	}
}

export function getBotInviteUrl(): string {
	return `https://discord.com/api/oauth2/authorize?client_id=${client.user.id}&permissions=0&scope=bot%20applications.commands`
}

export async function stopBot(): Promise<void> {
	mainLogger.info('Stopping bot...')
	await client.destroy()
}

async function guildCleanup() {
	await getConnection().transaction(async tx => {
		const guildIds = (await tx.createQueryBuilder()
			.select('"guildId"')
			.from(Preferences, 'Preferences')
			.getRawMany<{ guildId: Snowflake }>())
			.map(r => r.guildId)

		const guildIdsWhereBotLeft = guildIds.filter(id => !client.guilds.has(id))

		for (const id of guildIdsWhereBotLeft) {
			await onGuildDelete(id, tx);
		}
	})
}

async function onInteraction(interaction: GatewayInteractionCreateDispatchData) {
	// max allowed response delay
	const createdTimestamp = Number(DiscordSnowflake.deconstruct(interaction.id).timestamp)
	const remainingTime = createdTimestamp + 4000 - Date.now()

	await execInContext(interaction, async ctx => {
		if (remainingTime < 0) {
			ctx.logger.warn('received an expired interaction (expired since %dms)', -remainingTime)
			return
		}

		try {
			if (interaction.type === InteractionType.ApplicationCommand && interaction.data.type === ApplicationCommandType.ChatInput) {
				await processCommandInteraction(ctx as CmdContext)
			} else if (interaction.type === InteractionType.MessageComponent) {
				await processComponentInteraction(ctx as ComponentContext)
			} else if ( interaction.type === InteractionType.ModalSubmit) {
				await processModalInteraction(ctx as ModalContext)
			}
		} catch (e) {
			const reactionTime = Date.now() - createdTimestamp
			ctx.logger.error('encountered an error while processing the interaction (reaction time: %dms):\n', reactionTime, e)
			ctx.logger.error('contextual info about the previous error:\ninteraction: %s', shallowDump(ctx.interaction, 2))
		}
	})
}

function onGuildDelete(guildId: Snowflake, tx: EntityManager) {
	guildDeleteLogger.info(`Leaving guild ${guildId}, deleting preference data.`)
	tx.delete(Preferences, guildId).catch(err => {
		guildDeleteLogger.error(err)
	})
}

export function toggleMaintenance() {
	maintenance = !maintenance
}

export function isMaintenanceEnabled() {
	return maintenance
}
