# Connect 4

[![Discord Bots](https://top.gg/api/widget/status/761292824695013416.svg?noavatar=true)](https://top.gg/bot/761292824695013416)
[![Discord Bots](https://top.gg/api/widget/servers/761292824695013416.svg?noavatar=true)](https://top.gg/bot/761292824695013416)
[![Discord Bots](https://top.gg/api/widget/upvotes/761292824695013416.svg?noavatar=true)](https://top.gg/bot/761292824695013416)

Connect 4 is a Discord bot that allows you to play ranked games of connect 4.

## How to invite the bot to your server

Simply visit the [top.gg bot page](https://top.gg/bot/761292824695013416) and click on the “invite” button.

## How to self-host the bot

If you are a hard-core nerd, you can host your own instance of the bot by following the procedure bellow.

### Prerequisites

* Have `bsdtar` installed

* Have a running PostgreSQL database

* Have a `config.yml` file at the root of the project with the following structure:

```yaml
# optional (defaults to NODE_ENV === 'development')
dev_mode: false
# optional (defaults to dev_mode ? 'debug' : 'info')
# possible values: trace, debug, info, warn, error, fatal
log_level: 'trace'
bot_token: 'my_discord_bot_token'
default_game_over_img:
  win: https://discord-connect-4.gitlab.io/connect-4/win.jpg
  tie: https://discord-connect-4.gitlab.io/connect-4/tie.jpg
database:
  host: 'localhost'
  port: 5432
  name: 'connect_4_database'
  username: 'connect_4_user'
  # Don't use that password... really.
  password: 'P@55w0rd'
  # must be set to true for the first run (will create the database structure)
  sync: false
  # optional (defaults to devMove)
  log: false
# can also be set to false
support_server:
  invite_code: 'QQaFKWy94f'
# can also be set to false
topgg:
  token: 'my_topgg_token'
  webhook_auth: 'my_topgg_webhook_authorization_string'
```

* Have the dependencies installed:

```bash
pnpm install
```

* Build the program:

```bash
pnpm run build
```

### Run

* Set the `NODE_ENV` environment variable to `development` if you aren't running in production.

* Run the following:

```bash
pnpm run run
```

## Debugging hints

### Simulate a webhook call

```shell
curl -i -X POST -H "Content-Type: application/json" \
  -d '{"bot":"761292824695013416","user":"347315660679938060","type":"test","query":"?test=data&notRandomNumber=8","isWeekend":false}' \
  -H "Authorization: my_topgg_webhook_authorization_string" \
  http://localhost:8080/topgg-webhook
```

### Almost fill a board

```sql
UPDATE Game SET board = x'01479E472CEA9D08A3' WHERE id = ?;
```

### List all server custom discs

```ts
client.on('messageCreate', async msg => {
    if (msg.content !== '!') {
        return
    }
    let txt = ''
    const emojis = (msg.channel as GuildChannel).guild.emojis.cache;
    for (const [id, emoji] of emojis) {
        txt += `${emoji.toString()} \`<:_:${id}>\`\n`
    }
    await msg.channel.send(txt)
})
```

## Dependencies breakdown

| dependency          | reason                                                     |
|:--------------------|:-----------------------------------------------------------|
| @discordjs/builders | directly required                                          |
| @discordjs/rest     | directly required                                          |
| @discordjs/ws       | directly required                                          |
| @formatjs/intl      | directly required                                          |
| @sapphire/snowflake | directly required                                          |
| @top-gg/sdk         | directly required                                          |
| ajv                 | directly required                                          |
| base-x              | directly required                                          |
| bufferutil          | optional dependency @discordjs/ws (websocket optimization) |
| discord-api-types   | directly required                                          |
| express             | directly required                                          |
| inquirer            | directly required                                          |
| log4js              | directly required                                          |
| pad-buffer          | directly required                                          |
| pg                  | required by typeorm to connect to the DB                   |
| reflect-metadata    | required by typeorm to work with decorators                |
| superagent          | directly required                                          |
| timsort             | directly required                                          |
| typeorm             | directly required                                          |
| utf-8-validate      | optional dependency @discordjs/ws (websocket optimization) |
| yaml                | directly required                                          |
| zlib-sync           | optional dependency @discordjs/ws (websocket optimization) |
