import * as log4js from 'log4js'
import { config } from './config'

log4js.configure({
	appenders: {
		out: {
			type: 'stdout',
			layout: { type: 'pattern', pattern: '%[[%d] [%p] %X{ctxName} -%] %m' },
		},
	},
	categories: {
		default: {
			appenders: ['out'],
			level: config.log_level,
		},
	},
})

export function getLogger(ctxName: string): log4js.Logger {
	const logger = log4js.getLogger()
	logger.addContext('ctxName', ctxName)
	return logger
}

export const mainLogger = getLogger('main')
