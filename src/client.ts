import { REST } from '@discordjs/rest'
import { CompressionMethod, WebSocketManager, WebSocketShardEvents } from '@discordjs/ws'
import assert from 'assert/strict'
import { isMaintenanceEnabled } from './bot'
import { Context } from './context'
import {
	APIDMChannel,
	APIInteraction,
	APIInteractionResponseCallbackData,
	APIInteractionResponseChannelMessageWithSource,
	APIInteractionResponseDeferredChannelMessageWithSource,
	APIInteractionResponseUpdateMessage,
	APIModalInteractionResponse,
	APIModalInteractionResponseCallbackData,
	APIUser,
	GatewayActivityUpdateData,
	GatewayDispatchEvents,
	GatewayDispatchPayload,
	GatewayIntentBits,
	GatewayReadyDispatchData,
	InteractionResponseType,
	PresenceUpdateStatus,
	RESTGetAPIUserResult,
	RESTPatchAPIApplicationCommandJSONBody,
	RESTPatchAPIApplicationCommandResult,
	RESTPostAPIApplicationCommandsJSONBody,
	RESTPostAPIApplicationCommandsResult,
	RESTPostAPIChannelMessageJSONBody,
	RESTPutAPIApplicationCommandsJSONBody,
	RESTPutAPIApplicationCommandsResult,
	Routes,
	Snowflake
} from './discordApiTypes'
import { CompleteGuildMember, ControlledGuilds, Guilds } from './guilds'
import { mainLogger } from './logger'
import { parallelForEach } from './utils'

export type GatewayEventCallback
	<
		Event extends GatewayDispatchEvents = GatewayDispatchEvents,
		// this should be kept as default, it's here just to help the type engine a bit
		Payload extends GatewayDispatchPayload = GatewayDispatchPayload & { t: Event },
	>
	= (data: Payload['d']) => PromiseLike<any> | any

export interface ClientConfig {
	botToken: string
	intents: GatewayIntentBits
	activity: GatewayActivityUpdateData
}

export class Client {
	public readonly guilds: Guilds

	private readonly rest: REST
	private readonly webSocketManager: WebSocketManager

	private _user: APIUser | null = null
	private _applicationId: string | null = null
	private _shardCount: number | null = null

	private readyShards = new Set<number>()
	private readyCallback: (() => void) | null = null
	private gatewayEventCallbacks: { [Event in GatewayDispatchEvents]?: GatewayEventCallback<Event>[] } = {}

	/** guilds for which we're waiting to receive the GUILD_CREATE event */
	private loadingGuilds = new Set<Snowflake>()

	public constructor(config: ClientConfig) {
		this.rest = new REST().setToken(config.botToken)
		this.webSocketManager = new WebSocketManager({
			token: config.botToken,
			intents: config.intents,
			rest: this.rest,
			initialPresence: {
				status: PresenceUpdateStatus.Online,
				activities: [config.activity],
				since: null,
				afk: false,
			},
			compression: CompressionMethod.ZlibStream,
		})

		this.webSocketManager.on(WebSocketShardEvents.Debug, e => mainLogger.trace(e))
		this.webSocketManager.on(WebSocketShardEvents.Dispatch, async event => {
			const payload: GatewayDispatchPayload = event.data
			await this.processGatewayPayload(payload)
		})

		this.on(GatewayDispatchEvents.Ready, readyData => this.onShardReady(readyData))
		this.on(GatewayDispatchEvents.GuildCreate, g => this.onGuildReceived(g.id))
		this.on(GatewayDispatchEvents.GuildDelete, g => this.onGuildReceived(g.id))

		this.guilds = new ControlledGuilds(this)
	}

	public async connect(): Promise<void> {
		mainLogger.info('Connecting to the Gateway.')
		const onReady = new Promise<void>(resolve => this.readyCallback = resolve)
		await this.webSocketManager.connect()
		await onReady
		this.readyCallback = null
	}

	public destroy(): PromiseLike<void> | void {
		return this.webSocketManager.destroy()
	}

	public on<Event extends GatewayDispatchEvents>(event: Event, callback: GatewayEventCallback<Event>) {
		let callbacks: GatewayEventCallback<Event>[] | undefined = this.gatewayEventCallbacks[event]

		if (callbacks === undefined) {
			callbacks = this.gatewayEventCallbacks[event] = []
		}

		callbacks.push(callback)
	}

	public get user(): APIUser {
		if (this._user === null) {
			throw new Error('client isn\'t ready')
		}
		return this._user
	}

	public get applicationId(): string {
		if (this._applicationId === null) {
			throw new Error('client isn\'t ready')
		}
		return this._applicationId
	}

	public get shardCount(): number {
		if (this._shardCount === null) {
			throw new Error('client isn\'t ready')
		}
		return this._shardCount
	}

	public async fetchUser(id: Snowflake): Promise<RESTGetAPIUserResult> {
		return <RESTGetAPIUserResult> await this.rest.get(Routes.user(id))
	}

	public async fetchGuildMember(guildId: Snowflake, userId: Snowflake): Promise<CompleteGuildMember> {
		return <CompleteGuildMember> await this.rest.get(Routes.guildMember(guildId, userId))
	}

	public async fetchGuildMembers(guildId: Snowflake): Promise<CompleteGuildMember[]> {
		const limit = 1000
		let after = 0n

		let members: CompleteGuildMember[] = []

		let batch: CompleteGuildMember[]
		do {
			const query = new URLSearchParams()
			query.set('limit', limit.toString(10))
			query.set('after', after.toString(10))
			batch = <CompleteGuildMember[]> await this.rest.get(Routes.guildMembers(guildId), { query })
			if (batch.length === 0) {
				break
			}
			members = [...members, ...batch]
			after = batch.map(m => BigInt(m.user.id)).reduce((a, b) => a > b ? a : b)
		} while (batch.length === limit)

		return members
	}

	public async fetchSlashCommands(): Promise<RESTPutAPIApplicationCommandsResult> {
		return <RESTPutAPIApplicationCommandsResult> await
			this.rest.get(Routes.applicationCommands(this.applicationId))
	}

	public async createSlashCommand(slashCommand: RESTPostAPIApplicationCommandsJSONBody): Promise<RESTPostAPIApplicationCommandsResult> {
		return <RESTPostAPIApplicationCommandsResult> await
			this.rest.post(Routes.applicationCommands(this.applicationId), { body: slashCommand })
	}

	public async modifySlashCommand(slashCommandId: Snowflake, slashCommand: RESTPatchAPIApplicationCommandJSONBody): Promise<RESTPatchAPIApplicationCommandResult> {
		return <RESTPatchAPIApplicationCommandResult> await
			this.rest.patch(Routes.applicationCommand(this.applicationId, slashCommandId), { body: slashCommand })
	}

	public async bulkOverwriteSlashCommands(slashCommands: RESTPutAPIApplicationCommandsJSONBody): Promise<RESTPutAPIApplicationCommandsResult> {
		return <RESTPutAPIApplicationCommandsResult> await
			this.rest.put(Routes.applicationCommands(this.applicationId), { body: slashCommands })
	}

	public async deleteSlashCommand(slashCommandId: Snowflake): Promise<void> {
		await this.rest.delete(Routes.applicationCommand(this.applicationId, slashCommandId))
	}

	public async interactionReply(ctx: Context, response: APIInteractionResponseCallbackData): Promise<void> {
		const body: APIInteractionResponseChannelMessageWithSource = {
			type: InteractionResponseType.ChannelMessageWithSource,
			data: response,
		}
		await this.rest.post(Routes.interactionCallback(ctx.interaction.id, ctx.interaction.token), { body })

		if (isMaintenanceEnabled()) {
			await this.interactionFollowUp(ctx.interaction, {
				content: ctx.p.t('utils.maintenance'),
				flags: response.flags,
			})
		}
	}

	public async interactionShowModal(interaction: APIInteraction, modal: APIModalInteractionResponseCallbackData): Promise<void> {
		const body: APIModalInteractionResponse = {
			type: InteractionResponseType.Modal,
			data: modal,
		}
		await this.rest.post(Routes.interactionCallback(interaction.id, interaction.token), { body })
	}

	public async slashCmdInteractionDeferReply(interaction: APIInteraction, deferData: APIInteractionResponseDeferredChannelMessageWithSource['data']): Promise<void> {
		const body: APIInteractionResponseDeferredChannelMessageWithSource = {
			type: InteractionResponseType.DeferredChannelMessageWithSource,
			data: deferData,
		}
		await this.rest.post(Routes.interactionCallback(interaction.id, interaction.token), { body })
	}

	public async componentInteractionUpdateMsg(interaction: APIInteraction, response: APIInteractionResponseCallbackData): Promise<void> {
		const body: APIInteractionResponseUpdateMessage = {
			type: InteractionResponseType.UpdateMessage,
			data: response,
		}
		await this.rest.post(Routes.interactionCallback(interaction.id, interaction.token), { body })
	}

	public async interactionEditReply(ctx: Context, response: APIInteractionResponseCallbackData): Promise<void> {
		await this.rest.patch(Routes.webhookMessage(this.applicationId, ctx.interaction.token), { body: response })

		if (isMaintenanceEnabled()) {
			await this.interactionFollowUp(ctx.interaction, {
				content: ctx.p.t('utils.maintenance'),
				flags: response.flags,
			})
		}
	}

	public async interactionFollowUp(interaction: APIInteraction, response: APIInteractionResponseCallbackData): Promise<void> {
		await this.rest.post(Routes.webhook(this.applicationId, interaction.token), { body: response })
	}

	public async sendDM(userId: Snowflake, msg: RESTPostAPIChannelMessageJSONBody): Promise<void> {
		const dmChannel = <APIDMChannel> await this.rest.post(Routes.userChannels(), { body: { recipient_id: userId }})
		await this.rest.post(Routes.channelMessages(dmChannel.id), { body: msg })
	}

	private async processGatewayPayload<Event extends GatewayDispatchEvents>(payload: GatewayDispatchPayload & { t: Event }): Promise<void> {
		const event: Event = payload.t
		const callbacks: GatewayEventCallback<Event>[] | undefined = this.gatewayEventCallbacks[event]

		if (callbacks === undefined) {
			return
		}

		await parallelForEach(callbacks, async cb => {
			try {
				await cb(payload.d)
			} catch (e) {
				mainLogger.error('caught error on gateway event callback', e)
			}
		})
	}

	private onShardReady(data: GatewayReadyDispatchData) {
		assert(data.shard !== undefined)
		const [shardId, shardCount] = data.shard

		this._applicationId = data.application.id
		this._user = data.user
		this._shardCount = shardCount
		this.readyShards.add(shardId)

		for (const guild of data.guilds) {
			this.loadingGuilds.add(guild.id)
		}

		mainLogger.info(`Shard #${shardId} is ready (${this.readyShards.size}/${shardCount} shards ready in total).`)
		this.checkReady()
	}

	private onGuildReceived(guildId: Snowflake) {
		this.loadingGuilds.delete(guildId)
		this.checkReady()
	}

	private checkReady() {
		if (this.readyShards.size === this.shardCount && this.loadingGuilds.size === 0 && this.readyCallback !== null) {
			this.readyCallback()
		}
	}
}
