import { unlink } from 'fs/promises'
import net from 'net'

interface IPCReqPayload {
	nonce: number,
	functionName: string,
	arguments: any[],
}

interface IPCResPayload {
	nonce: number,
	error: boolean,
	result?: any,
}

const ipcServers: {[key: string]: net.Server} = {}
const clientSockets: {[key: string]: net.Socket} = {}

export async function startIPCServer (path: string, server: object): Promise<void> {
	if (path in ipcServers) {
		throw new Error('An IPC server is already started on this path: ' + path)
	}

	// clean unclosed Unix domain socket
	try {
		await unlink(path)
	} catch (err) {
		if (err.code !== 'ENOENT') {
			throw err
		}
	}

	// create server
	const ipcServer = ipcServers[path] = net.createServer()

	// wait for the server being up
	await new Promise<void>(resolve => ipcServer.listen(path, resolve))

	ipcServer.on('connection', socket => {
		socket.on('data', rawData => {
			const payload = JSON.parse(rawData.toString()) as IPCReqPayload
			const func = (server as {[key: string]: (...args: unknown[]) => Promise<unknown>})[payload.functionName]

			func.apply(server, payload.arguments).then(result => {
				socket.write(JSON.stringify(<IPCResPayload> {
					nonce: payload.nonce,
					error: false,
					result: result,
				}))
			}).catch(e => {
				console.error('IPC server side error occurred', e) // TODO logger
				socket.write(JSON.stringify(<IPCResPayload> {
					nonce: payload.nonce,
					error: true,
				}))
			})
		})
	})
}

export async function stopIPCServer(path: string): Promise<void> {
	if (!(path in ipcServers)) {
		throw new Error('No IPC server found on this path: ' + path)
	}

	const ipcServer = ipcServers[path]
	delete ipcServers[path]

	await new Promise<void>((resolve, reject) => {
		ipcServer.close(err => {
			if (err) {
				reject(err)
			} else {
				resolve()
			}
		})
	})
}

export async function connectToIPCServer<T> (path: string, Server: new () => T): Promise<T> {
	if (path in clientSockets) {
		throw new Error('A IPC client is already connected to this path: ' + path)
	}

	const socket = clientSockets[path] = await new Promise<net.Socket>(resolve => {
		const socket: net.Socket = net.connect(path, () => resolve(socket))
	})

	socket.on('data', rawData => {
		const payload = JSON.parse(rawData.toString()) as IPCResPayload

		const listener = responsesListeners[payload.nonce]
		delete responsesListeners[payload.nonce]

		if (payload.error) {
			listener.reject(new Error('IPC server side error occurred'))
		} else {
			listener.resolve(payload.result)
		}
	})

	let nonce = 0
	const responsesListeners: {
		[nonce: number]: {
			resolve: (value: any) => void,
			reject: (reason?: any) => void,
		}
	} = {}

	const serverMock = {} as any
	const methods = Object.getOwnPropertyNames(Server.prototype).filter(x => x !== 'constructor')

	for (const method of methods) {
		serverMock[method] = (...args: any[]): Promise<any> => {
			return new Promise((resolve, reject) => {
				const currentNonce = nonce++
				responsesListeners[currentNonce] = {resolve, reject}
				socket.write(JSON.stringify(<IPCReqPayload> {
					nonce: currentNonce,
					functionName: method,
					arguments: args,
				}), err => {
					if (err) {
						delete responsesListeners[currentNonce]
						reject(err)
					}
				})
			})
		}
	}

	return serverMock as T
}

export async function disconnectFromIPCServer (path: string): Promise<void> {
	if (!(path in clientSockets)) {
		throw new Error('No open IPC connection found on this path: ' + path)
	}

	const clientSocket = clientSockets[path]
	delete clientSockets[path]

	await new Promise<void>(resolve => clientSocket.end(resolve))
}
