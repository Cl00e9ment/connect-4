import { config } from '../config'
import { createConnection, getConnection } from 'typeorm'
import 'reflect-metadata'
import { readFile } from 'fs/promises'
import path from 'path'

export async function connectToDB(): Promise<void> {
	await createConnection({
		type: 'postgres',
		host: config.database.host,
		port: config.database.port,
		username: config.database.username,
		password: config.database.password,
		database: config.database.name,
		entities: [
			__dirname + '/entities/*.js',
		],
		synchronize: config.database.sync,
		logging: config.database.log,
	})

	await getConnection().transaction(async transaction => {
		await transaction.query('DROP FUNCTION IF EXISTS feed()')
		await transaction.query(await readFile(path.resolve(__dirname, '../../sql/feed.sql'), 'utf-8'))
	})
}
