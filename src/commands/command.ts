import { ActionRowBuilder, ButtonBuilder, inlineCode, SlashCommandBuilder } from '@discordjs/builders'
import { BtnContext, CmdContext, Context, MenuContext, ModalContext } from '../context'
import {
	APIActionRowComponent,
	APIButtonComponent,
	ApplicationCommandOptionType,
	ButtonStyle
} from '../discordApiTypes'
import { Game } from '../orm/entities/Game'
import { Preferences } from '../orm/entities/Preferences'
import { mentionCommand } from '../utils'
import { getCommand } from './manager'

// caution: don't use string enums without updating the nbOfCommandType formula
export enum CommandType {
	GAME,
	UTILITY,
	CUSTOMIZATION,
	MANAGEMENT, // No actual commands have the MANAGEMENT type, because these features were moved to the web UI.
}

// the formula is explained here: https://stackoverflow.com/a/38034825/9515617
export const nbOfCommandType = Object.keys(CommandType).length / 2

export interface Command {
	name: string
	type: CommandType
	usableIfBanned: boolean
	requireExternalEmojis: boolean
	/** defaults to false */
	guildOnly?: boolean
	defaultMemberPerms?: bigint

	buildOptions?(builder: SlashCommandBuilder, t: Preferences['t']): void
	exec(ctx: CmdContext): Promise<void>
	onBtnClicked?(ctx: BtnContext): Promise<void>
	onMenuInput?(ctx: MenuContext): Promise<void>
	onModalSubmit?(ctx: ModalContext): Promise<void>
}

export async function checkGameArg(ctx: CmdContext): Promise<{ error: string, game: null } | { error: null, game: Game }> {
	const gameIdOpt = ctx.interaction.data.options?.find(o => o.name === 'game_id')
	if (gameIdOpt === undefined || gameIdOpt.type !== ApplicationCommandOptionType.Integer) {
		throw new Error('failed to get the "game_id" option')
	}

	const gameId = gameIdOpt.value

	const game = !Game.isValidId(gameId) ? null : await ctx.tx.createQueryBuilder()
		.select('Game')
		.from(Game, 'Game')
		.leftJoinAndSelect('Game.player0', 'player0')
		.leftJoinAndSelect('Game.player1', 'player1')
		.where('Game.id = :id', { id: gameId })
		.andWhere('("player0Id" = :playerId OR "player1Id" = :playerId)', { playerId: ctx.userId })
		.getOne()

	if (!game) {
		return {
			error: ctx.p.t('cmd.game_action.game_not_found', { gameId }),
			game: null,
		}
	}

	if (game.gameOver) {
		return {
			error: ctx.p.t('cmd.game_action.game_terminated'),
			game: null,
		}
	}

	return { error: null, game }
}

export function organiseButtons(buttons: ButtonBuilder[]): APIActionRowComponent<APIButtonComponent>[] {
	const rows: APIActionRowComponent<APIButtonComponent>[] = []
	while (buttons.length > 0) {
		rows.push(new ActionRowBuilder<ButtonBuilder>().addComponents(buttons.splice(0, 5)).toJSON())
	}
	return rows
}

export function createPagination(nbOfPages: number, currentPageIndex: number): APIActionRowComponent<APIButtonComponent> {
	function createPageBtn(pageIndex: number, idSuffix?: string): ButtonBuilder {
		return new ButtonBuilder()
			.setCustomId('page' + pageIndex + (idSuffix ? '_' + idSuffix : ''))
			.setStyle(ButtonStyle.Primary)
			.setDisabled(pageIndex === currentPageIndex)
	}

	if (nbOfPages <= 5) {
		return new ActionRowBuilder<ButtonBuilder>()
			.addComponents(
				new Array(nbOfPages)
					.fill(null)
					.map((_, btnIndex) =>
						createPageBtn(btnIndex).setLabel((btnIndex + 1).toString())
					)
			)
			.toJSON()
	}

	return new ActionRowBuilder<ButtonBuilder>().addComponents(
		createPageBtn(0, 'first').setEmoji({ name: '⏮️' }),
		createPageBtn(Math.max(0, currentPageIndex - 1), 'prev').setEmoji({ name: '◀️' }),
		createPageBtn(currentPageIndex).setLabel((currentPageIndex + 1).toString()),
		createPageBtn(Math.min(nbOfPages - 1, currentPageIndex + 1), 'next').setEmoji({ name: '▶️' }),
		createPageBtn(nbOfPages - 1, 'last').setEmoji({ name: '⏭️' }),
	).toJSON()
}

export function getPageIndexFromBtnId(id: string): number {
	const match = /^page(\d+)(?:_.+)?$/.exec(id)
	if (!match) {
		throw new Error('failed to parse button customId: ' + id)
	}

	return parseInt(match[1], 10)
}

export function getCommandSynopsis(commandName: string, ctx: Context): string {
	const command = getCommand(commandName)
	let synopsis: string = mentionCommand(commandName)

	const subCommands = []
	const args = []

	if (command.buildOptions) {
		const builder = new SlashCommandBuilder()
			.setName('mock')
			.setDescription('mock')

		command.buildOptions(builder, x => ctx.p.t(x))

		for (const opt of builder.toJSON().options ?? []) {
			if (opt.type === ApplicationCommandOptionType.Subcommand) {
				subCommands.push(opt.name)
			} else if (opt.required) {
				args.push(inlineCode(opt.name))
			} else {
				args.push('(' + inlineCode(opt.name) + ')')
			}
		}
	}

	if (args.length > 0 && subCommands.length > 0) {
		throw new Error('can\'t process command having sub-commands and arguments at the same time')
	} else if (args.length > 0) {
		return mentionCommand(commandName) + ' ' + args.join(' ')
	} else if (subCommands.length > 0) {
		return subCommands
			.map(subCmd => mentionCommand(commandName, subCmd))
			.join('\n')
	}

	return synopsis
}
