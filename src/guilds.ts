import assert from 'assert/strict'
import { Client } from './client'
import {
	APIChannel,
	APIGuild, APIGuildMember,
	APIRole, APIUser,
	ChannelType, GatewayChannelModifyDispatchData,
	GatewayDispatchEvents,
	GatewayGuildCreateDispatchData,
	OverwriteType,
	PermissionFlagsBits,
	Snowflake
} from './discordApiTypes'

export type CompleteGuildMember = APIGuildMember & { user: APIUser }

export interface GuildInfo {
	readonly id: Snowflake
	readonly name: string
	readonly everyoneRoleId: Snowflake

	hasEveryoneExtEmojisPerm(channelId: Snowflake): boolean
	getExtEmojisPermDenyOrigin(channelId: Snowflake): 'guild' | 'channel' | 'guild_and_channel' | 'none'
}

/** like GuildInfo, but with logic to keep it updated */
class ControlledGuildInfo implements GuildInfo {
	public readonly id: Snowflake
	private _name: string

	/** whether the UseExternalEmojis permission is granted to @everyone */
	private extEmojisAllowed: boolean

	/** list of channel IDs where the UseExternalEmojis permission is overridden to grant it to @everyone  */
	private extEmojisOverrideAllow: Set<Snowflake>

	/** list of channel IDs where the UseExternalEmojis permission is overridden to deny it to @everyone  */
	private extEmojisOverrideDeny: Set<Snowflake>

	public constructor(createData: GatewayGuildCreateDispatchData) {
		this.id = createData.id
		this._name = createData.name
		this.extEmojisAllowed = hasExtEmojisPerm(everyoneRole(createData).permissions)
		this.extEmojisOverrideAllow = new Set()
		this.extEmojisOverrideDeny = new Set()

		for (const chan of createData.channels) {
			assert(chan.type !== ChannelType.DM && chan.type !== ChannelType.GroupDM)
			this.addOrUpdateChannel(chan)
		}
	}

	public update(updateData: APIGuild) {
		this._name = updateData.name
	}

	public updateRole(role: APIRole) {
		if (role.id === this.everyoneRoleId) {
			this.extEmojisAllowed = hasExtEmojisPerm(role.permissions)
		}
	}

	public addOrUpdateChannel(chan: GuildChannel) {
		this.extEmojisOverrideAllow.delete(chan.id)
		this.extEmojisOverrideDeny.delete(chan.id)

		const overwrites = chan.permission_overwrites
		if (overwrites === undefined) {
			return
		}

		const overwrite = overwrites.find(o => o.type === OverwriteType.Role && o.id === this.everyoneRoleId)
		if (overwrite === undefined) {
			return
		}

		if (hasExtEmojisPerm(overwrite.allow)) {
			this.extEmojisOverrideAllow.add(chan.id)
		} else if (hasExtEmojisPerm(overwrite.deny)) {
			this.extEmojisOverrideDeny.add(chan.id)
		}
	}

	public deleteChannel(chanId: Snowflake) {
		this.extEmojisOverrideAllow.delete(chanId)
		this.extEmojisOverrideDeny.delete(chanId)
	}

	public get everyoneRoleId(): Snowflake {
		return this.id // the @everyone role has the same ID as its guild
	}

	public hasEveryoneExtEmojisPerm(channelId: Snowflake): boolean {
		if (this.extEmojisAllowed) {
			return !this.extEmojisOverrideDeny.has(channelId);
		} else {
			return this.extEmojisOverrideAllow.has(channelId);
		}
	}

	public getExtEmojisPermDenyOrigin(channelId: Snowflake): 'guild' | 'channel' | 'guild_and_channel' | 'none' {
		if (this.extEmojisAllowed) {
			if (this.extEmojisOverrideDeny.has(channelId)) {
				return 'channel'
			} else {
				return 'none'
			}
		} else {
			if (this.extEmojisOverrideDeny.has(channelId)) {
				return 'guild_and_channel'
			} else {
				return 'guild'
			}
		}
	}

	public get name(): string {
		return this._name
	}
}

export type Guilds = ReadonlyMap<Snowflake, GuildInfo>

/** like Guilds, but mutable and with logic to keep itself updated */
export class ControlledGuilds extends Map<Snowflake, ControlledGuildInfo> {
	public constructor(client: Client) {
		super()
		client.on(GatewayDispatchEvents.GuildCreate, g => this.set(g.id, new ControlledGuildInfo(g)))
		client.on(GatewayDispatchEvents.GuildUpdate, g => this.getExisting(g.id).update(g))
		client.on(GatewayDispatchEvents.GuildDelete, g => this.delete(g.id))
		client.on(GatewayDispatchEvents.GuildRoleUpdate, r => this.getExisting(r.guild_id).updateRole(r.role))
		client.on(GatewayDispatchEvents.ChannelCreate, filterGuildChannels(c => this.getExisting(c.guild_id).addOrUpdateChannel(c)))
		client.on(GatewayDispatchEvents.ChannelUpdate, filterGuildChannels(c => this.getExisting(c.guild_id).addOrUpdateChannel(c)))
		client.on(GatewayDispatchEvents.ChannelDelete, filterGuildChannels(c => this.getExisting(c.guild_id).deleteChannel(c.id)))
	}

	private getExisting(id: Snowflake): ControlledGuildInfo {
		const guild = this.get(id)
		assert(guild !== undefined)
		return guild
	}
}

// some utility functions

function hasExtEmojisPerm(permissions: string): boolean {
	return (BigInt(permissions) & (PermissionFlagsBits.UseExternalEmojis | PermissionFlagsBits.Administrator)) !== 0n
}

function everyoneRole(guild: APIGuild): APIRole {
	const role = guild.roles.find(r => r.id === guild.id) // the @everyone role has the same ID has its guild
	assert(role !== undefined)
	return role
}

type GuildChannel = Exclude<APIChannel, { type: ChannelType.DM | ChannelType.GroupDM }>

type GatewayGuildChannelModifyDispatchData =
	Exclude<GatewayChannelModifyDispatchData, { type: ChannelType.DM | ChannelType.GroupDM }> & { guild_id: Snowflake }

/**
 * Wraps a guild channel create/update/delete callback into a generic channel create/update/delete callback.
 * If the channel fed to the returned callback isn't a guild channel, the wrapped callback will not be called.
 */
function filterGuildChannels(
	callback: (chan: GatewayGuildChannelModifyDispatchData) => Promise<void> | void
): (chan: GatewayChannelModifyDispatchData) => Promise<void> | void {
	return (chan) => {
		if (chan.type === ChannelType.DM || chan.type === ChannelType.GroupDM) {
			return
		}
		assert(chan.guild_id !== undefined)
		return callback(
			// nonsense to make the type engine understand what we're doing
			// this can be replaced by just `chan`
			{ ...chan, guild_id: chan.guild_id }
		)
	}
}
