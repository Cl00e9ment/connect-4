import { startBot, stopBot } from './bot'
import { initIntl } from './intl'
import { connectToDB } from './orm/connection'
import { startIPCServer, stopIPCServer } from './adminTools/ipc'
import { adminToolsSocketPath, Backend } from './adminTools/backend'
import { loadCommands } from './commands/manager'
import { mainLogger } from './logger'

async function main() {
	process.env.TZ = 'UTC'
	mainLogger.info('Starting the program.')
	await connectToDB()
	await initIntl()
	await startBot()
	await loadCommands()
	await startIPCServer(adminToolsSocketPath, new Backend())
	process.on('SIGINT', () => {
		(async () => {
			await stopIPCServer(adminToolsSocketPath)
			await stopBot()
		})()
			.then(() => process.exit(0))
			.catch(() => process.exit(1))
	})
}

main().catch(err => {
	mainLogger.error(err)
	mainLogger.error('Exiting the program due to an error in the main context.')
	process.exit(1)
})
